//
// g e t _ v e c t o r : check that argument is a vector;
// return it if so.

//#define DEBUG

#include "ocst.h"

ColumnVector get_vector(const octave_value& arg, const std::string& caller, 
	int argnum,int& errflg)
{

  #ifdef DEBUG
  cout << "get_vector: entry from " << caller << " argument " 
       << argnum << ", errflg=" << errflg << endl;
  #endif
  ColumnVector retval(0);
  ostrstream errmesg;

  if(arg.is_empty() )
    return retval;

  if( 
    !( 
      arg.is_numeric_type() 
      && (
        arg.is_matrix_type() || arg.is_scalar_type() 
      ) 
    )
  )
  {
    errmesg << caller << "expecting real vector for argument " 
	<< argnum << ends;
    error(errmesg.str());
    errflg = 1;
    return retval;
  }
  else if( ! ( (arg.rows() == 1) || (arg.columns() == 1) ) )
  {
    errmesg << caller << "argument " << argnum << " (" 
	<< arg.rows() << "x" << arg.columns() << ") must be a vector" 
	<< ends;
    error(errmesg.str());
    errflg = 1;
    return retval;
  }
  else 
  {
    #ifdef DEBUG
    cout << "get_vector: normal return. Vector is" 
	<< arg.rows() << "x" << arg.columns()  << "; value="
	<< arg.vector_value() << endl;
    #endif
    Matrix myval = arg.matrix_value();
    int ii;
    if(arg.rows() == 1)
      {
        retval = ColumnVector( arg.columns() );
        for( ii = 0 ; ii < arg.columns() ; ii++)
          retval(ii) = myval(0,ii);
      }
    else
      {
        retval = ColumnVector( arg.rows() );
        for( ii = 0 ; ii < arg.rows() ; ii++)
          retval(ii) = myval(ii,0);
      }
    return retval;
  }
}

