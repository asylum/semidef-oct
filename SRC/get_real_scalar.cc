#include "ocst.h"

//
// g e t _ r e a l _ s c a l a r :
//
// signflg: 0: any real scalar is o.k.
//          1: nonnegative only
//          2: positive only

double get_real_scalar(const octave_value& arg, const std::string& caller,
	int argnum, int signflg, int& errflg)
{
  double retval = 0;
  ostrstream errmesg;
  
  if ( !( arg.is_real_type() && arg.is_scalar_type() ) )
  {
    errmesg << caller  << "argument " << argnum 
	<< " must be a real scalar" << ends;
    error(errmesg.str());
    errflg = 1;
    return retval;
  }
  retval = arg.double_value();
  switch(signflg)
  {
  case NONNEGATIVE:		// nonnegative
    if ( retval < 0 )
    {
      errmesg << caller  << "argument " << argnum
	<< " = " << retval 
        << " must be a nonnegative real scalar" << ends;
      error(errmesg.str());
      errflg = 1;
    }
    break;
  case POSITIVE:
    if ( retval <= 0 )
    {
      errmesg << caller  << "argument " << argnum
	<< " = " << retval 
        << " must be a positive real scalar" << ends;
      error(errmesg.str());
      errflg = 1;
    }
  }

  return retval;
}

