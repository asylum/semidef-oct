mopt = 1;
while(mopt >0)
 mopt =  menu( ...
   "Octave implementation of Vandenberghe and Boyd's semidef package", ...
   "educational testing problem", ...
   "linear programming", ...
   "matrix norm minimization", ...
   "stability margin problem", ...
   "quit");
 switch(mopt)
 case(1),
   printf("\neducational testing problem: maximize the trace of a diagonal\n");
   printf("matrix D > 0 such that A - D >=0 for a given positive definite\n");
   printf("matrix A\n\n");
   cmd = "help etp";
   run_cmd
   cmd = "a = [2 -0.1 -1 ; -0.1 3 0.2 ; -1 0.2 4]";
   run_cmd
   cmd = "[d,Q] = etp(a)";
   run_cmd
   cmd = "eig_a_minus_diag_d = eig(a - diag(d))";
   run_cmd
 case(2),
   printf("\nlinear programming problem:\n\n");
   cmd = "help lin_prog";
   run_cmd
   cmd = "a = [-1 -1 ; 2 1; 0 1]";
   run_cmd
   cmd = "b = [1;2;3]";
   run_cmd
   cmd = "c = [-1 1]";
   run_cmd
   cmd = "[x,z,info] = lin_prog(a,b,c)";
   run_cmd
   cmd = "Ax_plus_b= a*x+b";
   run_cmd
 case(3),
   printf("\nmatrix norm minimization problem:\n\n");
   cmd = "help mtrx_nrm";
   run_cmd
   cmd = "A0 = [2 3; -1 2 ; 4 -1]";
   run_cmd
   cmd = "A1 = [0 1; 0 0 ; 0 0]";
   run_cmd
   cmd = "A2 = [0 0; 0 0 ; 1 0]";
   run_cmd
   cmd = "a = [A0 A1 A2]";
   disp(cmd);
   eval(cmd);
   cmd = "[x,Q] = mtrx_nrm(a,2)";
   run_cmd
   cmd = "norm_A0 = norm(A0)";
   run_cmd
   cmd = "norm_Ax= norm(A0 + x(1)*A1 + x(2)*A2)";
   run_cmd
 case(4),
   printf("\nstability margin problem:\n\n");
   disp("not done yet");
   prompt
 case(5),
   mopt = 0;
 otherwise,
   error(sprintf("illegal value of mopt=%f",mopt));
 endswitch
endwhile

