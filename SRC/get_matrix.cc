//
// g e t _ m a t r i x : check that argument is a real matrix;
// return it if so.

#include "ocst.h"

//#define DEBUG

Matrix get_matrix(const octave_value& arg, const std::string& caller, 
	int argnum, int& errflg)
{
  Matrix retval(0,0);

  if(arg.is_empty() )
    return retval;

  if( !( arg.is_numeric_type() 
	&& (arg.is_matrix_type() || arg.is_scalar_type ())) )
  {
    ostrstream errmesg;
    errmesg << caller << "expecting real matrix for argument" 
	<< argnum << ends;
    error(errmesg.str());
    errflg = 1;
    return retval;
  }
  else
  {
    retval = arg.matrix_value();
    #ifdef DEBUG
    cout << "get_matrix: returning with dimensions " 
	<< retval.rows() << "x" << retval.columns() << endl;
    #endif
    return retval;
  }
}

