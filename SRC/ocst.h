// system structure library functions

#ifndef _SYSLIB_H_
#define _SYSLIB_H_

#include <octave/config.h>

#include <iostream.h>
#include <math.h>
#include <string.h>
#include <strstream.h>
#include <utils.h>

#include <octave/defun-dld.h>
#include <octave/error.h>
#include <octave/gripes.h>
#include <octave/help.h>
#include <octave/oct-obj.h>
#include <octave/oct-map.h>
#include <octave/ov.h>
#include <octave/pager.h>
#include <octave/symtab.h>
#include <octave/variables.h>

#define REAL 0
#define NONNEGATIVE 1
#define POSITIVE 2

// OCST C++ callable functions
ColumnVector abcddim(const octave_value_list &);
string_vector& add_name(string_vector&, const std::string&, int);
octave_value_list fir2sys(const octave_value_list&);
ComplexColumnVector get_complex_vector(const octave_value&, const std::string&,
	int,int&);
octave_value get_key(const octave_value& arg, const std::string& caller, 
	const std::string& key, int argnum,int& errflg);
Matrix get_matrix(const octave_value&, const std::string&, int, int&);
double 
get_real_scalar(const octave_value &, // usually args(idx)
	const std::string &,		      // calling routine name
	int,			      // argument number (usually idx+1)
	int,			      // REAL, NONNEGATIVE or POSITIVE
	int&);			      // error flag

inline double get_real_scalar(const octave_value& arg, 
	const std::string& name, int argnum, int& errflg)
{
  return get_real_scalar(arg,name,argnum,REAL,errflg); 
}
inline double get_nonnegative_real_scalar(const octave_value& arg, 
	const std::string& name, int argnum, int& errflg)
{
  return get_real_scalar(arg,name,argnum,NONNEGATIVE,errflg); 
}
inline double get_positive_real_scalar(const octave_value& arg, 
	const std::string& name, int argnum, int& errflg)
{
  return get_real_scalar(arg,name,argnum,POSITIVE,errflg); 
}

std::string get_string(const octave_value&, const std::string& , int, int&);
string_vector get_string_matrix(const octave_value&, const std::string&, int, int&);
ColumnVector  get_vector(const octave_value &, const std::string&, int, int&);
bool is_digital(const octave_value_list&);
inline bool is_square(const octave_value& mat)
{
  return (mat.rows() == mat.columns()) ? true : false;
}
ostream& operator<<(ostream&,const string_vector&);
Octave_map pack(const ColumnVector&, const string_vector&, const string_vector&,
	double tsam=1);						//FIR
Octave_map pack(const Matrix&, const Matrix&, const Matrix&, const Matrix&,
	double, int, int, const string_vector&, const string_vector&,
	const string_vector&, const ColumnVector&);		//SS
// pack transfer-function form data into system structure
Octave_map pack(const ColumnVector&, const ColumnVector&, 
	const string_vector&, const string_vector&, double tsam=0);		// TF
// pack transfer-function form data into system structure
Octave_map pack(const ComplexColumnVector&, const ComplexColumnVector&, 
	double, const string_vector&, const string_vector&, double tsam=0);	//ZP

octave_value_list ss2sys(const octave_value_list&);
string_vector sysdefioname(int,const std::string&);
string_vector sysdefstname(int,int);
octave_value_list sysout(const octave_value_list&);
octave_value_list tf2sys(const octave_value_list&);
Matrix zero_matrix(int,int);	// return a zero matrix of specified dimensions
octave_value_list zp2sys(const octave_value_list&);

#endif
