# Semidefinite Programming functions for GNU Octave

Semidef-oct is a port to Octave by Alan S. Hodel a.s.hodel@eng.auburn.edu of the semidef package by Lieven Vandenberghe vandenbe@esat.kuleuven.ac.be and Stephen Boyd boyd@isl.stanford.edu.

Alan S. Hodel died in 2009[1,2], the code is unmaintained since then.
[1] http://www.eng.auburn.edu/elec/staff/hodel-memorial.html
[2] http://www.legacy.com/obituaries/OANow/obituary.aspx?n=Dr-Alan-Scottedward-Hodel&pid=122771920

The original semidef-code is licensed under LGPL[3,4], the code from Alan Hodel is GPL2+.
[3] http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=435214#45
[4] http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=435214#55

The code was uploaded to gitorious.org on 2012-04-23 and moved to gitlab.com after the latter acquired gitorious.org.
