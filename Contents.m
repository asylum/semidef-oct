%
% Software for semidefinite programming.  
% Beta version. 
% Copyright 1994 by Vandenberghe and Boyd.
%
% Primary routines.
%   sp        - solve SP and its dual (MEX-files and a help M-file).
%   bigM      - solve SP with constraint Tr F(x) <= M.
%   phase1    - look for feasible point.
%
% Example applications. 
%   etp       - solve educational testing problem.
%   log_cheb  - solve log. Chebychev approx. problem.
%   lin_prog  - solve the linear program: min c'*x, A*x + b >= 0.
%   mtrx_nrm  - solve matrix norm minimization problem.
%
